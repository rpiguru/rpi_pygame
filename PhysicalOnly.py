#Honda Graphics
#importing required libraries
import RPi.GPIO as GPIO
import time
import math
#activating Broadcom chip
GPIO.setmode(GPIO.BCM)
#assigning GPIO pins to functions
#assigning pin 18 to 1 point
point1 = 18
GPIO.setup(point1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#assigning pin 23 to 3 point
point3 = 23
GPIO.setup(point3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#assigning pin 24 to 5 point
point5 = 24
GPIO.setup(point5, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#assigning pin 21 to  finish (reset switch)
finish = 21
GPIO.setup(finish, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#assigning pin 20 to  start
starting = 20
GPIO.setup(starting, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#track power out pin
backward = 4
GPIO.setup(backward, GPIO.OUT)
GPIO.output(backward,False)
#track reverse out pin
forward = 17
GPIO.setup(forward, GPIO.OUT)
GPIO.output(forward,False)
#assiging relay power
relay = 26
GPIO.setup(relay, GPIO.OUT)
GPIO.output(relay,True)
#Variables
trackrun = 0
trackstop = 0
#game loop
try:
    while True:
            #1 Point scored timer modifier
        if GPIO.input(point1) == False:
   #                     print("Point1")
                        trackrun = trackrun + 2
    #                    print (trackrun)
                        time.sleep (.7)                       
            #3 Point scored timer modifier
        if GPIO.input(point3) == False:
    #                    print("Point3")
                        trackrun = trackrun + 4
     #                   print (trackrun)
                        time.sleep (.7)
            #5 Point scored timer modifier
        if GPIO.input(point5) == False:
 #                       print("Point5")
                        trackrun = trackrun + 6
  #                      print (trackrun)
                        time.sleep (.7)                        
            #Finishline sensor
        if GPIO.input(finish) == False:
 #                       print("Finish")
                        trackrun = 0
                        GPIO.output(backward, True)
                        time.sleep (8)
            #starting lineup sensor
        if GPIO.input(starting) == False:
#                       print("Start")
                        GPIO.output(backward, False)
                        trackrun = trackrun + 1.1

        if trackrun > trackstop:
                        GPIO.output(forward, True)
                        trackrun = (trackrun - 1)
                        time.sleep(.1)
        if trackrun <= 0:
                       GPIO.output(forward,False)
                       trackrun = 0
                       print (trackrun)
        time.sleep(0.1)

except KeyboardInterrupt:
  print (" Wiping Pins")
  GPIO.cleanup() 
   








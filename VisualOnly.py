#Honda Graphics



#importing required libraries
import RPi.GPIO as GPIO
import pygame
import time
import math
from pygame.locals import*

framerate = pygame.time.Clock()
fps = 10


#activating Broadcom chip
GPIO.setmode(GPIO.BCM)



#assigning GPIO pins to functions

#assigning pin 18 to 1 point
point1 = 18
GPIO.setup(point1, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#assigning pin 23 to 3 point
point3 = 23
GPIO.setup(point3, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#assigning pin 24 to 5 point
point5 = 24
GPIO.setup(point5, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#assigning pin 21 to  finish (reset switch)
finish = 21
GPIO.setup(finish, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#assigning pin 20 to  start
starting = 20
GPIO.setup(starting, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#track power out pin
backward = 4
GPIO.setup(backward, GPIO.OUT)
GPIO.output(backward,False)
#track reverse out pin
forward = 17
GPIO.setup(forward, GPIO.OUT)
GPIO.output(forward,False)

relay = 26
GPIO.setup(relay, GPIO.OUT)
GPIO.output(relay,True)


#declaring variables
whiteColor = pygame.Color(255,255,255)
blackColor = pygame.Color(0,0,0)
trackrun = 0
trackstop = 0
stopwatch = False
ms = 0
s = 0
m = 0 


#initializing pygame
pygame.init()

    
#setting display size
screenWidth = 1600
screenHeight = 1200
windowSurfaceObj = pygame.display.set_mode((screenWidth,screenHeight),1,24)
timerWidth = 900
timerHeight = 400
timerX = 400
timerY = 850
logoHeight = 1000
logoWidth = 1300
logoX = 155
logoY = 60


#painting main display color
#screen.fill ((whiteColor))
windowSurfaceObj.fill((whiteColor))
pygame.display.update()


#naming the screen
pygame.display.set_caption('HONDA')
#loading HondaLogo
logo = pygame.image.load('/home/pi/Desktop/HondaLogo2.png').convert()
logo = pygame.transform.scale(logo,(logoWidth,logoHeight))
windowSurfaceObj.blit(logo,(logoX,logoY))
pygame.display.update()


#setting the timer visual
fontObj = pygame.font.Font('/home/pi/Desktop/freesansbold.ttf',200)
msg = "00:00:00"
msgSurfaceObj = fontObj.render(msg, False,blackColor)
msgRectobj = msgSurfaceObj.get_rect()
msgRectobj.topleft =(timerX,timerY)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
windowSurfaceObj.blit(msgSurfaceObj, msgRectobj)
pygame.display.update()



#game loop
try:

    while True:


        framerate.tick(fps)

        if stopwatch == True:

                        sw = (m,":",s,":",ms)
                        ms += 17.5
                        if ms >= 100:
                            s += 1
                            ms = 0
                        if s >= 60:
                            m += 1
                            s = 0
                        sw = (m,s,ms)
#                        print (sw)



                        msg= "%02d:%02d:%02d" % (m,s,ms)
                        pygame.draw.rect(windowSurfaceObj,whiteColor,Rect(timerX,timerY,timerWidth,timerHeight))
                        msgSurfaceObj = fontObj.render(msg, False,blackColor)
                        msgRectobj = msgSurfaceObj.get_rect()
                        msgRectobj.topleft =(timerX,timerY)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                        windowSurfaceObj.blit(msgSurfaceObj, msgRectobj)
                        pygame.display.update()


            #1 Point scored timer modifier
        if GPIO.input(point1) == False:
   #                     print("Point1")
#                        trackrun = trackrun + 2
    #                    print (trackrun)
                        stopwatch = True
 #                       time.sleep (.55)

                        
            #3 Point scored timer modifier
        if GPIO.input(point3) == False:
    #                    print("Point3")
              #          trackrun = trackrun + 4
     #                   print (trackrun)
                        stopwatch = True
                 #       time.sleep (.55)
                        

            #5 Point scored timer modifier
        if GPIO.input(point5) == False:
 #                       print("Point5")
                      #  trackrun = trackrun + 6
  #                      print (trackrun)
                        stopwatch = True
                      #  time.sleep (.55)

                        
            #Finishline sensor
        if GPIO.input(finish) == False:

 #                       print("Finish")
                     #   trackrun = 0
                        GPIO.output(backward, True)
                        stopwatch = False
                     #   time.sleep (10)

            #starting lineup sensor
        if GPIO.input(starting) == False:
#                       print("Start")
                        GPIO.output(backward, False)
                        m = 0
                        s = 0
                        ms = 0
    #    if trackrun > trackstop:
       #                 GPIO.output(forward, True)
       #                 trackrun = (trackrun - 1)
#                        time.sleep (.01)

     #   if trackrun <= 0:
      #                 GPIO.output(forward,False)
      #                 trackrun = 0
       #                print (trackrun)
#      #                 time.sleep(.01)

   #     time.sleep(.1)





except KeyboardInterrupt:
  print (" Wiping Pins")
  GPIO.cleanup() 
   








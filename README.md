## Enable auto-starting

We assume that executable python scripts are in `/home/pi/Desktop` and their file name is `PhysicalOnly.py` & `VisualOnly.py`


1. Since `VisualOnly.py` needs GUI environment, we need to create bash shell script.

        cd ~
        mkdir script
        cd script
        nano gui_run.sh
        
    And add follow:
        
        #!/bin/bash
    
        source /home/pi/.profile
        /usr/bin/python /home/pi/Desktop/VisualOnly.py
    
    Press **CTRL+X**, and **Y** to save and exit.
    
    And make shell script executable.
    
        sudo chmod 775 gui_run.sh
    
2. Create log directory and enable auto-starting
    
- Create log directory
    
        mkdir /home/pi/logs
    
- Open `crontab`.

        sudo crontab -e
    
    And add follow:
        
        @reboot /usr/bin/python /home/pi/Desktop/PhysicalOnly.py >/home/pi/logs/cronlog 2>&1
        @reboot sh /home/pi/script/gui_run.sh >/home/pi/logs/cronlog 2>&1
    
    Press **CTRL+X**, and **Y** to save and exit.
    
- Open `/etc/rc.local`.
    
        sudo nano /etc/rc.local
    
    And add follow before `exit 0`:
        
        (/usr/bin/xinit /home/pi/script/gui_run.sh)&
    
    Press **CTRL+X**, and **Y** to save and exit.
    
- And reboot
        
        sudo reboot


REF: http://www.instructables.com/id/Raspberry-Pi-Launch-Python-script-on-startup/?ALLSTEPS
